module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            options: {
                separator: ';'
            },
            default: {
                src: ['public/vendor/jquery.min.js',
                    'public/vendor/jquery.simplemodal.min.js',
                    'public/javascript/script.js'],
                dest: 'public/javascript/<%= pkg.name %>.concat.js'
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
                preserveComments: false,
                mangle: false
            },
            default: {
                src: '<%= concat.default.dest %>',
                dest: 'public/javascript/<%= pkg.name %>.min.js'
            }
        },
        clean: [
            '<%= concat.default.dest %>',
            '<%= uglify.default.dest %>',
            '<%= cssmin.minify.src %>',
            '<%= cssmin.minify.dest %>'
        ],
        cssmin: {
            combine: {
                files: {
                    'public/stylesheets/<%= pkg.name %>.concat.css': [
                        'public/stylesheets/styles.css'
                    ]
                }
            },
            minify: {
                src: 'public/stylesheets/<%= pkg.name %>.concat.css',
                dest: 'public/stylesheets/<%= pkg.name %>.min.css'
            }
        },
        watch: {
            scripts: {
                files: ['**public/javascript/*.js'],
                tasks: ['concat','uglify'],
                options: {
                    spawn: false,
                }
            },
            styles: {
                files: ['**public/stylesheets/*.css'],
                tasks: ['cssmin'],
                options: {
                    spawn: false
                }
            }
        }
    });

    // Load the plugin that provides the "uglify" task.
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-watch');


    // Default task(s).
    grunt.registerTask('default', ['clean','cssmin','concat','uglify']);
    grunt.event.on('watch', function(action, filepath, target) {
        grunt.log.writeln(target + ': ' + filepath + ' has ' + action);
    });

};