<!DOCTYPE html>
<html ng-app>
    <head>
        <title>D3R - Test</title>
        <link rel="stylesheet" href="/stylesheets/d3r-test.min.css" type="text/css" />
    </head>
<body>

    <div ng-controller="SearchCtrl">
        <form class="well form-search">
            <label>Search:</label>
            <input type="text" ng-model="keywords" class="input-medium search-query" placeholder="Keywords...">
            <button type="submit" class="btn" ng-click="search()">Search</button>
            <p class="help-block">Try for example: "php" or "angularjs" or "asdfg"</p>
        </form>
    </div>

    <script src="/javascript/d3r-test.min.js"></script>
</body>
</html>